APP_STL := c++_static
NDK_TOOLCHAIN_VERSION := clang
APP_MODULES      := tiff png jpeg tifffactory tiffsaver tiffconverter
APP_CFLAGS := -D_FORTIFY_SOURCE=2 -fstack-protector-all
APP_CPPFLAGS := -D_FORTIFY_SOURCE=2 -fstack-protector-all
APP_LDFLAGS:= -WL,-z,relro,-z,now
APP_PLATFORM:=android-27

APP_ABI := all

#APP_OPTIM := debug